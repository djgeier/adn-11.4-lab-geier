﻿using System;

namespace Test_Driven_Development
{
    /// <summary>
    /// A class to represent an employee.
    /// </summary>
    [Serializable]
    public class Employee
    {
        /// <summary>
        /// The employee's hourly wage.
        /// </summary>
        private decimal hourlyWage;

        /// <summary>
        /// The number of hours that the employee is scheduled for.
        /// </summary>
        private int hoursScheduled;

        /// <summary>
        /// Initializes a new instance of the Employee class.
        /// </summary>
        /// <param name="hourlyWage">The employee's hourly wage.</param>
        /// <param name="hoursScheduled">The employee's scheduled number of hours.</param>
        public Employee(decimal hourlyWage, int hoursScheduled)
        {
            this.hourlyWage = hourlyWage;
            this.hoursScheduled = hoursScheduled;
        }

        /// <summary>
        /// Instantiates a new instance of the Employee class.
        /// </summary>
        private Employee()
        {
        }

        /// <summary>
        /// Gets or sets the employee's paycheck.
        /// </summary>
        public decimal Paycheck { get; set; }

        /// <summary>
        /// Has the employee do work.
        /// </summary>
        /// <param name="work">The job to perform.</param>
        public void DoWork(Job work)
        {
            decimal cost = 0;
            if (hoursScheduled <= work.TimeInvestmentRemaining)
            {
                cost = hourlyWage * hoursScheduled;
                this.Paycheck += cost;
                work.TimeInvestmentRemaining -= hoursScheduled;
                hoursScheduled = 0;
            }
            else
            {
                cost = work.TimeInvestmentRemaining * hourlyWage;
                this.Paycheck += cost;
                this.hoursScheduled -= work.TimeInvestmentRemaining;
                work.TimeInvestmentRemaining = 0;
            }

            work.JobCost += cost * 1.5m;
        }
    }
}