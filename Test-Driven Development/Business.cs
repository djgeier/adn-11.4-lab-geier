﻿using System;
using System.Collections.Generic;

namespace Test_Driven_Development
{
    /// <summary>
    /// A class to represent a business.
    /// </summary>
    [Serializable]
    public class Business
    {
        /// <summary>
        /// Instantiates a new instance of the Business class.
        /// </summary>
        public Business()
        {
            this.Employees = new List<Employee>();
            this.Jobs = new List<Job>();
        }

        /// <summary>
        /// The list of employees for the business.
        /// </summary>
        public List<Employee> Employees { get; set; }

        /// <summary>
        /// The list of jobs to complete.
        /// </summary>
        public List<Job> Jobs { get; set; }

        /// <summary>
        /// Adds an employee to the business.
        /// </summary>
        /// <param name="employee">The employee to add.</param>
        public void AddEmployee(Employee employee)
        {
            this.Employees.Add(employee);
        }

        /// <summary>
        /// Adds a job to the business.
        /// </summary>
        /// <param name="job">The job to add.</param>
        public void AddJob(Job job)
        {
            this.Jobs.Add(job);
        }

        /// <summary>
        /// Has the business perform all of their jobs.
        /// </summary>
        public void DoWork()
        {
            // For each job, if job isn't completed, check each employees to see if they have hours
            // and have them DoWork(). If job is completed after work, break from loop.
            foreach (Job j in this.Jobs)
            {
                if (!j.JobCompleted)
                {
                    foreach (Employee e in this.Employees)
                    {
                        e.DoWork(j);
                        if (j.JobCompleted)
                        {
                            break;
                        }
                    }
                }
            }
        }
    }
}