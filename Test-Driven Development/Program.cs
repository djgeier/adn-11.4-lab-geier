﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace Test_Driven_Development
{
    /// <summary>
    /// The main class of the program.
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// Serializes a business object to XML.
        /// </summary>
        /// <param name="b">The business to serialize.</param>
        public static void SerializeBusiness(Business b)
        {
            // Create a binary formatter
            XmlSerializer formatter = new XmlSerializer(typeof(Business));

            // Create a file using the passed-in file name Use a using statement to automatically
            // clean up object references and close the file handle when the serialization process
            // is complete
            using (Stream stream = File.Create(@"C:\ADN\business.xml"))
            {
                // Serialize (save) the current instance of the zoo
                formatter.Serialize(new StreamWriter(stream), b);
            }
        }

        /// <summary>
        /// The main method of the program.
        /// </summary>
        /// <param name="args">Command line arguments.</param>
        private static void Main(string[] args)
        {
            Business business = new Business();
            business.AddEmployee(new Employee(10, 10));
            business.AddEmployee(new Employee(10, 10));
            business.AddJob(new Job(15));
            business.DoWork();

            SerializeBusiness(business);
            Console.ReadLine();
        }
    }
}