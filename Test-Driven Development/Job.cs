﻿using System;

namespace Test_Driven_Development
{
    /// <summary>
    /// A class to represent a job.
    /// </summary>
    [Serializable]
    public class Job
    {
        /// <summary>
        /// Instantiates a new instance of the Job class.
        /// </summary>
        /// <param name="timeInvestment">The time it takes to complete the job.</param>
        public Job(int timeInvestment)
        {
            this.TimeInvestmentRemaining = timeInvestment;
        }

        /// <summary>
        /// Instantiates a new instance of the Job class.
        /// </summary>
        public Job()
        {
        }

        /// <summary>
        /// Gets a value that indicates whether the job is completed.
        /// </summary>
        public bool JobCompleted => this.TimeInvestmentRemaining == 0;

        /// <summary>
        /// Gets or sets the Job's cost.
        /// </summary>
        public decimal JobCost { get; set; }

        /// <summary>
        /// Gets or sets the time investment remaining.
        /// </summary>
        public int TimeInvestmentRemaining { get; set; }
    }
}