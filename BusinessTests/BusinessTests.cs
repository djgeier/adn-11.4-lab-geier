using Microsoft.VisualStudio.TestTools.UnitTesting;
using Test_Driven_Development;

namespace BusinessTests
{
    /// <summary>
    /// Several tests for the business program.
    /// </summary>
    [TestClass]
    public class BusinessTests
    {
        /// <summary>
        /// Tests the employee Paycheck field to see if the paycheck is computed properly.
        /// </summary>
        [TestMethod]
        public void CheckEmployeeCompensation()
        {
            // Arrange.
            Employee e = new Employee(10, 20);

            // Act.
            e.DoWork(new Job(20));

            // Assert
            Assert.AreEqual(200, e.Paycheck);
        }

        /// <summary>
        /// Tests the job cost field to see if the cost is computed properly.
        /// </summary>
        [TestMethod]
        public void CheckJobCost()
        {
            // Arrange.
            Employee e = new Employee(10, 20);
            Job j = new Job(10);

            // Act.
            e.DoWork(j);

            // Assert.
            Assert.AreEqual(150, j.JobCost);
        }

        /// <summary>
        /// Tests to see if the jobs are left incomplete if there are not enough hours to complete
        /// the job.
        /// </summary>
        [TestMethod]
        public void CheckSecondJobUnfinished()
        {
            // Arrange
            Business business = new Business();
            business.AddJob(new Job(15));
            business.AddJob(new Job(10));
            business.AddEmployee(new Employee(10, 15));

            // Act
            business.DoWork();

            // Assert
            Assert.IsFalse(business.Jobs[1].JobCompleted);
        }

        /// <summary>
        /// Tests to see if the employee is added to the list properly.
        /// </summary>
        [TestMethod]
        public void EmployeeSuccessfullyAddedToList()
        {
            // Arrange
            Business business = new Business();

            // Act
            business.AddEmployee(new Employee(10, 10));
            business.AddEmployee(new Employee(10, 10));

            // Assert
            Assert.AreEqual(2, business.Employees.Count);
        }

        /// <summary>
        /// Tests to see if jobs are added to the list.
        /// </summary>
        [TestMethod]
        public void JobAddedToListSuccessfullyTest()
        {
            // Arrange
            Business business = new Business();

            // Act
            business.AddJob(new Job(5));
            business.AddJob(new Job(10));

            // Assert
            Assert.AreEqual(2, business.Jobs.Count);
        }

        /// <summary>
        /// Tests to see if the jobs complete after the business works.
        /// </summary>
        [TestMethod]
        public void JobCompleteAfterWorkTest()
        {
            // Arrange
            Business business = new Business();
            business.AddJob(new Job(2));
            business.AddEmployee(new Employee(10, 5));

            // Act
            business.DoWork();

            // Assert
            Assert.IsTrue(business.Jobs[0].JobCompleted);
        }

        /// <summary>
        /// Tests to see if the job is left incomplete if there are no available employees.
        /// </summary>
        [TestMethod]
        public void JobIncompleteWhenOutOfEmployees()
        {
            // Arrange
            Business business = new Business();
            business.AddJob(new Job(15));
            business.AddEmployee(new Employee(10, 5));
            business.AddEmployee(new Employee(10, 7));

            // Act
            business.DoWork();

            // Assert
            Assert.IsFalse(business.Jobs[0].JobCompleted);
        }
    }
}